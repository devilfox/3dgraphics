#ifndef __FRAMEBUFFER_H__
#define __FRAMEBUFFER_H__
#include "coordinate.h"
class pixelcolorinfo{
    private:
    unsigned char red,green,blue;
    public:
    void set(int&,int&,int&);
    void set(pixelcolorinfo&);
    bool operator !=(pixelcolorinfo&);
};
namespace buffer{
    extern pixelcolorinfo* framebuffer;
    void loadframebuffer(void);
    void writeframebuffer(void);
    void setbufferpixel(int&,int&,pixelcolorinfo&);
};
#endif
