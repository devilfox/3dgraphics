#include "renderfunc.h"
#include "draw.h"
#include "pipeline.h"
#include "camera.h"

void renderfunc::sortvertex(vertex3d& v1,vertex3d& v2,vertex3d& v3){
    vertex3d temp;
    if(v1.y>v2.y){
        temp = v1; v1 = v2; v2 = temp;
    }
    if(v1.y > v3.y){
        temp = v1; v1 = v3; v3 = temp;
    }
    if(v2.y > v3.y){
        temp = v2;  v2 = v3;  v3 = temp;
    }
}

void renderfunc::flatbottomfill(vertex3d& v3,vertex3d& v2,vertex3d& v1){
    vertex3d ver1,ver2;
    ver1.color(v3.r,v3.g,v3.b);ver2.color(v3.r,v3.g,v3.b);

    float inverseSlope1 = (v2.x - v3.x) / (v2.y - v3.y);
    float inverseSlope2 = (v3.x - v1.x) / (v3.y - v1.y);
    float curx1 = v3.x;
    float curx2 = v3.x;


    for (int y = v3.y; y>v2.y;y--){
        ver1.x = curx1; ver1.y = y;
        ver2.x = curx2; ver2.y = y;
        renderfunc::extInterpolate(v3,v2,ver1);
        renderfunc::extInterpolate(v3,v1,ver2);
        draw::renderline2d(ver1,ver2);
        curx1 -= inverseSlope1;    curx2 -= inverseSlope2;
    }
}

void renderfunc::flattopfill(vertex3d& v3,vertex3d& v2,vertex3d& v1){
    vertex3d ver1,ver2;
    ver1.color(v3.r,v3.g,v3.b);ver2.color(v3.r,v3.g,v3.b);


    float invslope1 = (v1.x - v3.x) / (v1.y - v3.y);
    float invslope2 = (v1.x - v2.x) / (v1.y - v2.y);
    float curx1 = v1.x;
    float curx2 = v1.x;

    for (int y = v1.y; y<v2.y;y++){
        ver1.x = curx1; ver1.y = y;
        ver2.x = curx2; ver2.y = y;
        renderfunc::extInterpolate(v3,v1,ver1);
        renderfunc::extInterpolate(v2,v1,ver2);
        draw::renderline2d(ver1,ver2);
         curx1 += invslope1;
         curx2 += invslope2;
    }
}

void renderfunc::filltriangle(vertex3d v1,vertex3d v2,vertex3d v3){
    vertex3d v4;

    pipeline::___3dto2d(v1);
    pipeline::___3dto2d(v2);
    pipeline::___3dto2d(v3);
    pipeline::___3dto2d(v4);

    v4.color(v1.r,v1.g,v1.b);


    //draw::renderline2dwithextinterpolation(v1,v2);
    //draw::renderline2dwithextinterpolation(v2,v3);
    //draw::renderline2dwithextinterpolation(v1,v3);


    renderfunc::sortvertex(v1,v2,v3);
    if(v1.y == v2.y){renderfunc::flatbottomfill(v3,v2,v1);}
    else if(v2.y == v3.y){renderfunc::flattopfill(v3,v2,v1);}
    else{
        v4.y=v2.y;
        v4.x=v3.x+((v2.y-v3.y)/(v1.y-v3.y))*(v1.x-v3.x);
        renderfunc::extInterpolate(v3,v1,v4);
        draw::renderline2d(v2,v4);
        renderfunc::flatbottomfill(v3,v2,v4);
        renderfunc::flattopfill(v2,v4,v1);
    }
}


void renderfunc::extInterpolate(vertex3d v1, vertex3d v2,vertex3d& vr){
    //z buffer calculation
    //making v1.y always > v2.y
    vertex3d temp;
    if(v1.y<v2.y){
        temp = v1; v1 = v2; v2 = temp;
    }
    double c1,c2;
    c1 = (vr.y-v2.y)/(v1.y-v2.y);
    c2 = (v1.y-vr.y)/(v1.y-v2.y);
    vr.z = (c1)*v1.z + (c2)*v2.z;
    //std::cout<<vr.z<<std::endl;
    //intensity interplolation to be done below
    if(pipeline::enablelight){
        vr.r = c1*v1.r+c2*v2.r;
        vr.g = c1*v1.g+c2*v2.g;
        vr.b = c1*v1.b+c2*v2.b;
    }

}

void renderfunc::intInterpolate(vertex3d v1, vertex3d v2,int x,int y,double& z,int& r,int& g,int& b){
    //now order according to x as v1 should be with greater x
    //z calculation here
    vertex3d temp;
    if(v1.x>v2.x){
        temp = v1; v1 = v2; v2 = temp;
    }
    double c1,c2;
    c1 = float(v2.x-x)/float(v2.x-v1.x);
    c2 = float(x-v1.x)/float(v2.x-v1.x);
    z = (c1)*v1.z + (c2)*v2.z;
    //intensity calculations here
    if(pipeline::enablelight){
        r = c1*v1.r + c2*v2.r;
        g = c1*v1.g + c2*v2.g;
        b = c1*v1.b + c2*v2.b;
    }
}
