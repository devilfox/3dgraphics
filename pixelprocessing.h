#ifndef __PIXELPROCESSING_H__
#define __PIXELPROCESSING_H__
#include "coordinate.h"
#include <iostream>
#include <map>
#include <vector>
namespace processing{
    ///only pixelbuffer (not screen buffer), it will only contain the finalised pixels
    class processor{
        public:
            void addpixel(int,int,int,int,int,int);
            void buffertransfer(void);
        private:
            std::vector<pixel> fbuffer;
    };

    extern processor* intermediatebuffer;

    extern int a;
    ///intermediate screen buffer section
    extern std::map<int,pixel*> screenbuffer; //intermediate screen buffer
    void addpixel(int&,int&,double&,int&,int&,int&);//add pixel to the intermediate screen buffer
    void transfertoframebuffer(void);
};
#endif
