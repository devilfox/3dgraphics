#include <iostream>
#include "lighting.h"
#include "coordinate.h"
#include "camera.h"

LightSource::LightSource(){}

LightSource::LightSource(int x,int y,int z,float _r,float _g,float _b){
        position.x = x;
        position.y = y;
        position.z = z;
        r = _r;
        g = _g;
        b = _b;
        calculate_eye_coordinates();
    }

    void LightSource::setposition(int x,int y,int z)
    {
        position.x = x;
        position.y = y;
        position.z = z;

        calculate_eye_coordinates();
    }

    void LightSource::setposition(vertex3d pos)
    {
        position = pos;
        calculate_eye_coordinates();
    }

    void LightSource::setintensity(float _r,float _g,float _b)
    {
        r = _r;
        g = _g;
        b = _b;
    }

    void LightSource::calculate_eye_coordinates()
    {
        MatrixXd pos(4,1),mp(4,1);
        pos << position.x,position.y,position.z,1;
        mp = cam::modelviewmatrix * pos;
        modelled_position.x = mp(0,0);
        modelled_position.x = mp(1,0);
        modelled_position.x = mp(2,0);
    }

    vertex3d LightSource::getEyeCoordinates()
    {
        return modelled_position;
    }

    void LightSource::calculate_light_vector(float x,float y,float z)
    {
        light_vector.x = modelled_position.x - x;
        light_vector.y = modelled_position.y - y;
        light_vector.z = modelled_position.z - z;
        light_vector.normalize();
    }

    void LightSource::calculate_light_vector(vertex3d v)
    {
        light_vector.x = modelled_position.x - v.x;
        light_vector.y = modelled_position.y - v.y;
        light_vector.z = modelled_position.z - v.z;

        light_vector.normalize();
    }

namespace lighting{
    LightSource lightobj;
}
