#include "camera.h"
#include "draw.h"
#include "coordinate.h"
#include "buffer.h"
#include "pixelprocessing.h"
#include "pipeline.h"
#include "renderfunc.h"
#include <cmath>
void draw::line2d(pixel& p1,pixel& p2){
    int xa,ya,xb,yb,d; float dd;
    p1.unpack(d,d,d,xa,ya,dd);
    p2.unpack(d,d,d,xb,yb,dd);

    pixelcolorinfo f;
    int r =255,g=155,b=155;
    f.set(r,g,b);

    int dx=abs(xa-xb),dy=abs(yb-ya);
    int a,p;
    if(((xb-xa)<0&&(yb-ya)>0)||((yb-ya)<0&&(xb-xa)>0)) a=-1;
    else a=1;
    if(dy<dx) {
        int tmp;
        if(xa>xb){
            tmp=xa;  xa=xb;  xb=tmp;
            tmp=ya;  ya=yb;  yb=tmp;
        }
        p=2*dy-dx;
//        setPixels(xa,ya,clr);
            processing::intermediatebuffer->addpixel(xa,ya,1,255,255,255);

        while(xa<xb){
            if(p<0){ p+=2*dy; }
            else{
                p+=2*(dy-dx);
                ya+=a;
            }
            xa++;
           // setPixels(xa,ya,clr);
           processing::intermediatebuffer->addpixel(xa,ya,1,255,255,255);
        }
    }
    else{
        int tmp;
        p=2*dx-dy;
        if(ya>yb){
            tmp=xa; xa=xb;  xb=tmp;
            tmp=ya; ya=yb;  yb=tmp;
        }
//        setPixels(xa,ya,clr);
        processing::intermediatebuffer->addpixel(xa,ya,1,255,255,255);
        while(ya<yb){
            if(p<0) p+=2*dx;
            else{
                p+=2*(dx-dy);
                xa+=a;
            }
            ya++;
           // setPixels(xa,ya,clr);
        processing::intermediatebuffer->addpixel(xa,ya,1,255,255,255);
        }
    }
}

void draw::line3d(vertex3d& v1,vertex3d& v2){
    pixel p1,p2;
    pipeline::_3dto2d(v1);
//    v1.show();
    //std::cout << cam::out.x<<' '<<cam::out.y<<' '<<cam::out.z<<std::endl;
    p1.pack(255,255,255,cam::out.x,cam::out.y,cam::out.z);
    pipeline::_3dto2d(v2);
    p2.pack(255,255,255,cam::out.x,cam::out.y,cam::out.z);
  //  v2.show();
    //std::cout << cam::out.x<<' '<<cam::out.y<<' '<<cam::out.z<<std::endl;

    draw::line2d(p1,p2);

}


void draw::renderline2d(vertex3d p1,vertex3d p2){
if(p1.x==p2.x&&p1.y==p2.y) return;
vertex3d xp1 = p1,xp2 = p2;
int x,y,r=p1.r,g=p1.g,b=p1.b;
double z;

//std::cout<<"here";
//z,r,g,b will be interpolated for each value of (xa,ya)
    int dx=abs(p1.x-p2.x),dy=abs(p2.y-p1.y);
    int a,p;
    if(((p2.x-p1.x)<0&&(p2.y-p1.y)>0)||((p2.y-p1.y)<0&&(p2.x-p1.x)>0)) a=-1;
    else a=1;
    if(dy<dx) {
        int tmp;
        if(p1.x>p2.x){
            tmp=p1.x;  p1.x=p2.x;  p2.x=tmp;
            tmp=p1.y;  p1.y=p2.y;  p2.y=tmp;
        }
        p=2*dy-dx;
        x = p1.x; y = p1.y;
        renderfunc::intInterpolate(xp1,xp2,x,y,z,r,g,b);
        processing::addpixel(x,y,z,r,g,b);
        while(p1.x<p2.x){
            if(p<0){ p+=2*dy; }
            else{
                p+=2*(dy-dx);
                p1.y+=a;
            }
            p1.x++;
        x = p1.x; y = p1.y;
        renderfunc::intInterpolate(xp1,xp2,x,y,z,r,g,b);
        processing::addpixel(x,y,z,r,g,b);
        }
    }
    else{
        int tmp;
        p=2*dx-dy;
        if(p1.y>p2.y){
            tmp=p1.x; p1.x=p2.x;  p2.x=tmp;
            tmp=p1.y; p1.y=p2.y;  p2.y=tmp;
        }
        x = p1.x; y = p1.y;
        renderfunc::intInterpolate(xp1,xp2,x,y,z,r,g,b);
        processing::addpixel(x,y,z,r,g,b);
        while(p1.y<p2.y){
            if(p<0) p+=2*dx;
            else{
                p+=2*(dx-dy);
                p1.x+=a;
            }
            p1.y++;
        x = p1.x; y = p1.y;
        renderfunc::intInterpolate(xp1,xp2,x,y,z,r,g,b);
        processing::addpixel(x,y,z,r,g,b);
        }
    }
}

void draw::renderline2dwithextinterpolation(vertex3d p1,vertex3d p2){
    if(p1.x==p2.x&&p1.y==p2.y) return;
vertex3d xp1 = p1,xp2 = p2,dummy;
int x,y;
double z;

//std::cout<<"here";
//z,r,g,b will be interpolated for each value of (xa,ya)
    int dx=abs(p1.x-p2.x),dy=abs(p2.y-p1.y);
    int a,p;
    if(((p2.x-p1.x)<0&&(p2.y-p1.y)>0)||((p2.y-p1.y)<0&&(p2.x-p1.x)>0)) a=-1;
    else a=1;
    if(dy<dx) {
        int tmp;
        if(p1.x>p2.x){
            tmp=p1.x;  p1.x=p2.x;  p2.x=tmp;
            tmp=p1.y;  p1.y=p2.y;  p2.y=tmp;
        }
        p=2*dy-dx;
        dummy.x = p1.x; dummy.y = p1.y;
        renderfunc::extInterpolate(xp1,xp2,dummy);
        x = int(dummy.x); y = int(dummy.y); z = double(dummy.z);
        processing::addpixel(x,y,z,dummy.r,dummy.g,dummy.b);
        while(p1.x<p2.x){
            if(p<0){ p+=2*dy; }
            else{
                p+=2*(dy-dx);
                p1.y+=a;
            }
            p1.x++;
        dummy.x = p1.x; dummy.y = p1.y;
        renderfunc::extInterpolate(xp1,xp2,dummy);
        x = int(dummy.x); y = int(dummy.y); z = double(dummy.z);
        processing::addpixel(x,y,z,dummy.r,dummy.g,dummy.b);
        }
    }
    else{
        int tmp;
        p=2*dx-dy;
        if(p1.y>p2.y){
            tmp=p1.x; p1.x=p2.x;  p2.x=tmp;
            tmp=p1.y; p1.y=p2.y;  p2.y=tmp;
        }
        dummy.x = p1.x; dummy.y = p1.y;
        renderfunc::extInterpolate(xp1,xp2,dummy);
        x = int(dummy.x); y = int(dummy.y); z = double(dummy.z);
        processing::addpixel(x,y,z,dummy.r,dummy.g,dummy.b);
        while(p1.y<p2.y){
            if(p<0) p+=2*dx;
            else{
                p+=2*(dx-dy);
                p1.x+=a;
            }
            p1.y++;
        dummy.x = p1.x; dummy.y = p1.y;
        renderfunc::extInterpolate(xp1,xp2,dummy);
        x = int(dummy.x); y = int(dummy.y); z = double(dummy.z);
        processing::addpixel(x,y,z,dummy.r,dummy.g,dummy.b);
        }
    }
}

void draw::renderline3d(vertex3d v1,vertex3d v2){
    pipeline::_3dto2d(v1);
    v1.x = cam::out.x; v1.y = cam::out.y; v1.z = cam::out.z;
    pipeline::_3dto2d(v2);
    v2.x = cam::out.x; v2.y = cam::out.y; v2.z = cam::out.z;
    draw::renderline2d(v1,v2);
}
