#ifndef __SHADOW_H__
#define __SHADOW_H__
#include "coordinate.h"
#include <iostream>
#include <cstring>
#include <vector>
namespace shadow{
extern std::string plane;
extern std::vector<std::string> objects;
//3 points on the plane in anticlockwise direction
//Initialise it with value on the planes
extern vertex3d points_on_plane[3];

//An arbitrary point on plane
extern vertex3d arb_point;

//Position of Light source
extern vertex3d light_pos;

//Normal to the plane A,B,C

extern std::vector<vertex3d> endpoints;

extern vertex2d ep1,ep2;

struct Plane_normal
{
    float A;
    float B;
    float C;
};


void getplane(void);

void setarb_point(void);

bool isShadowEnabledOn(std::string);
//Calculation of Normal to the plane using 3 points on plane
Plane_normal determine_plane_coefficients(vertex3d pt[]);
//Calculation of shadow coordinates
void calculate_shadow_coordinates(vertex3d vertex,vertex3d& shade);

bool isShadowWithinPlane(int,int);
}
#endif
