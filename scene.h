#ifndef __SCENE_H__
#define __SCENE_H__
namespace scene{
    void wireframe(void);
    void renderedscene(void);
    void rendershadow(void);
}
#endif
