#include "scene.h"
#include "coordinate.h"
#include "objectreader.h"
#include "draw.h"
#include "renderfunc.h"
#include "pipeline.h"
#include "shadowcalculation.h"
#include <ctime>
void scene::wireframe(void){
    std::vector<object>::iterator it;
    std::vector<face>::iterator _it;
    for(it=model::loader->objects.begin();it!=model::loader->objects.end();++it){
        object obj = *it;
        for(_it=obj.faces.begin();_it!=obj.faces.end();++_it){
            face f = *_it;
            if(f.v.size()==3){
                draw::line3d(f.v[0],f.v[1]);
                draw::line3d(f.v[1],f.v[2]);
                draw::line3d(f.v[0],f.v[2]);
            }else if(f.v.size()==2){
				draw::line3d(f.v[0],f.v[1]);
            }
        }
    }
}

void scene::renderedscene(void){
//int a=0;
std::vector<object>::iterator it;
    std::vector<face>::iterator _it;
    for(it=model::loader->objects.begin();it!=model::loader->objects.end();++it){
        object obj = *it;
        for(_it=obj.faces.begin();_it!=obj.faces.end();++_it){
            face f = *_it;
            if(f.v.size()==3){

//                srand(time(NULL));
//                f.v[0].r = f.v[1].r=f.v[2].r = rand()%255+a++;
//                f.v[0].g = f.v[1].g=f.v[2].g = rand()%255+a++;
//                f.v[0].b = f.v[1].b=f.v[2].b = rand()%255+a++;
                //std::cout<<"here\n "<<f.v[0].r<<f.v[0].g<<f.v[0].b<<std::endl;
                renderfunc::filltriangle(f.v[0],f.v[1],f.v[2]);
            }else if(f.v.size()==2){
				draw::line3d(f.v[0],f.v[1]);
            }
        }
    }
    if(pipeline::enablelight && pipeline::enableshadow){
        pipeline::shadowCodeBegin = true;
        scene::rendershadow();
        pipeline::shadowCodeBegin = false;
    }
}

void scene::rendershadow(){
    //pipeline::enablelight = false;
    vertex3d v1,v2,v3;
    v1.color(0,0,0); v2.color(0,0,0); v3.color(0,0,0);
    std::vector<object>::iterator it;
    std::vector<face>::iterator _it;
    for(it=model::loader->objects.begin();it!=model::loader->objects.end();++it){
        object obj = *it;
        if(shadow::isShadowEnabledOn(obj.objname)){
            for(_it=obj.faces.begin();_it!=obj.faces.end();++_it){
                face f = *_it;
                if(f.v.size()==3){
                    shadow::calculate_shadow_coordinates(f.v[0],v1);
                    shadow::calculate_shadow_coordinates(f.v[1],v2);
                    shadow::calculate_shadow_coordinates(f.v[2],v3);
                    renderfunc::filltriangle(v1,v2,v3);
                }
            }
        }
    }
    pipeline::enablelight = true;
}
