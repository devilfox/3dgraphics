#ifndef __vertex_h__
#define __vertex_h__

#include <iostream>
#include <vector>
#include <cmath>

struct pixel{
    uint8_t r,g,b;
    float z;
    int x,y;
    pixel(){}
    pixel(const int& _r,const int& _g,const int& _b,const int& _x,const int& _y,const float& _z){
        r = _r; g = _g; b = _b; z = _z;
        x = _x; y = _y;
    }
    void pack(const int& _r,const int& _g,const int& _b,const int& _x,const int& _y,const float& _z){
        r = _r; g = _g; b = _b; z = _z;
        x = _x; y = _y;
    }

    void unpack(int& _r,int& _g,int& _b,int& _x,int& _y,float& _z){
        _r = r; _g = g; _b = b; _z = z; _x = x; _y = y;
    }
};

struct vertex3d{
    float x,y,z;//coordinate
    float nx,ny,nz; //normal of the vertex
     int r,g,b; //vertex color
    vertex3d(){ x=y=z=nx=ny=nz=0;r=g=b=255;}
    vertex3d(float _x,float _y,float _z){
        x = _x; y = _y; z = _z;
    }
    void color(int _r,int _g,int _b){
        r = _r; g = _g; b = _b;
    }
    void show(){
        std::cout << "point: "<<'('<<x<<','<<y<<','<<z<<')'<<"  normal"<<'('<<nx<<','<<ny<<','<<nz<<')'<<" color"<<'('<<r<<','<<g<<','<<b<<')'<<std::endl;
    }
    void normalize(){
        float dnom = sqrt(x*x+y*y+z*z);
        x = x/dnom; y = y/dnom; z = z/dnom;
    }
    void coords(float _x,float _y,float _z){
        x = _x; y = _y; z = _z;
    }

};

struct vertex2d{
    int x,y;
};

struct vertex3dn{
    float x,y,z; //vertex
    float vnx,vny,vnz; //vertex normal
};


struct face{
 std::vector<int> vid; //vertex id
 std::vector<vertex3d> v;
 vertex3d n;
};

struct object{
std::string objname;
 int r,g,b;
 std::vector<face> faces;
 void color(int _r,int _g,int _b){
    r = _r; g = _g; b = _b;
 }
};

struct tracker{ //this will help normalising the faces
    int encounters;
    vertex3d v;
    tracker(){}
    tracker(vertex3d& v3){
        encounters=0;
        v.x = v3.x; v.y=v3.y; v.z=v3.z;
        v.nx = v.ny = v.nz = 0;
    }
    void update(vertex3d n3){
        encounters++;
        v.nx += n3.x; v.ny += n3.y; v.nz += n3.z;
    }
    void average(void){
        v.nx = v.nx/encounters;
        v.ny = v.ny/encounters;
        v.nz = v.nz/encounters;
    }

};
/*struct Vertex{
    unsigned int __1__; //[8bit r][8bit g][8bit b][8bit z]
    unsigned int __2__; //[12bit x][12bit ][8 bit redundent]
    void pack(int x,int y,int z,int r,int g,int b){
    //x,y,z will not cross the value of 2046
    //r,g,b is values between 0-255
        __1__ = (r<<24)|(g<<16)|(b<<8)|z;
        __2__ = (x << 20)|(y<<8);
    }
    void unpack(int& x, int& y, int& z, int &r,int& g, int& b){
        z = __1__&255;
        b = (__1__ >> 8)&255;
        g = (__1__ >> 16)&255;
        r = (__1__ >> 24)&255;
        y = (__2__ >> 8)&4095;
        x = (__2__ >> 20)&4095;
    }
};*/


#endif


