#ifndef __DRAW_H__
#define __DRAW_H__
#include "coordinate.h"
namespace draw{
    void line2d(pixel&,pixel&);
    void line3d(vertex3d&,vertex3d&);
    void renderline2d(vertex3d,vertex3d);
    void renderline2dwithextinterpolation(vertex3d,vertex3d);
    void renderline3d(vertex3d,vertex3d);
    void rendertriangle(std::vector<vertex3d>&,int&,int&,int&); //vertex,r,g,b
};
#endif
