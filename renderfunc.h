#ifndef __RENDERFUNC_H__
#define __RENDERFUNC_H__
#include "coordinate.h"
namespace renderfunc{
    void sortvertex(vertex3d&,vertex3d&,vertex3d&);
    void flatbottomfill(vertex3d&,vertex3d&,vertex3d&);
    void flattopfill(vertex3d&,vertex3d&,vertex3d&);
    void filltriangle(vertex3d,vertex3d,vertex3d);


    void extInterpolate(vertex3d,vertex3d,vertex3d&); //(v1,v2,put value to v3)
    void intInterpolate(vertex3d,vertex3d,int,int,double&,int&,int&,int&); //(v1,v2,put value to v3)
};
#endif
