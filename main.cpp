#include <iostream>
#include<GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "buffer.h"
#include "pixelprocessing.h"
#include "coordinate.h"
#include "pipeline.h"
#include "camera.h"
#include "objectreader.h"
#include "draw.h"
#include "scene.h"
#include "shadowcalculation.h"
using namespace std;

bool verbose = false;
bool flag = false;

vertex3d v1,v2;

void keyEvent(unsigned char key, int x, int y){
 switch(key){

    case 'r':
        if(pipeline::rotateangle<0)
            pipeline::rotateangle=-pipeline::rotateangle;
        pipeline::rotateScene();
        break;
    case 'R':
        if(pipeline::rotateangle>0)
            pipeline::rotateangle=-pipeline::rotateangle;
        pipeline::rotateScene();
    case 'l':
        pipeline::rotateLight();
        break;


    case 'n':
        cam::eye.x = 8;
        cam::eye.y = 10;
        cam::eye.z = 20;
    break;

    case 'f':
        cam::eye.x = 20;
        cam::eye.y = 25;
        cam::eye.z = 28;
        break;

    case 'x':
        cam::eye.x+=2;
        std::cout<<cam::eye.x<<std::endl;
        break;
    case 'X':
        cam::eye.x-=2;
        std::cout<<cam::eye.x<<std::endl;
        break;
 }
 pipeline::adjustchange();
 glutPostRedisplay();
}

void keyboard(int key,int x,int y)
{
    if(key == GLUT_KEY_DOWN){
        cam::light.z+=5;
    }else if(key==GLUT_KEY_UP){
        cam::light.z-=5;
    }else if(key==GLUT_KEY_LEFT){
        cam::light.x-=5;
    }else if(key==GLUT_KEY_RIGHT){
        cam::light.x+=5;
    }
 pipeline::adjustchange();
 glutPostRedisplay();
}

void drawings(void){
    glClear(GL_COLOR_BUFFER_BIT);
    buffer::loadframebuffer();
            scene::renderedscene();
        processing::transfertoframebuffer();
    buffer::writeframebuffer();
    glutSwapBuffers();
}

int main(int argc, char **argv){
    pipeline::init();
    pipeline::initialisewindow(800,600);
    pipeline::intitialvalidvalues();
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(cam::screenw,cam::screenh);
    glutInitWindowPosition(100,100);
    glutCreateWindow("Basketball Court");
    glutKeyboardFunc(keyEvent);
    glutSpecialFunc(keyboard);
    glutDisplayFunc(drawings);
    glClearColor(0.13f,0.55f,0.69f,1.0f);
    glViewport(0, 0,cam::screenw,cam::screenh);
    glutMainLoop();
    return 0x00;
}

