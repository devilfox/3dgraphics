#ifndef __OBJLOADER_H__
#define __OBJLOADER_H__
#include <iostream>
#include <fstream>
#include <cstring>
#include <map>
#include "coordinate.h"
namespace model{
    class objloader{
        public:
            bool load(std::string);
            object getObject(std::string);
            void getcolor(std::string,int&,int&,int&);
            std::vector<object> objects;
            std::map<int,tracker*> vertextracker;
        private:
            bool has(std::string&,std::string);
            void resolvenormals(void);

    };
    extern objloader* loader;
};
#endif
