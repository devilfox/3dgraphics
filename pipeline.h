#ifndef __pipeline_H__
#define __pipeline_H__
#include "coordinate.h"
extern bool verbose;
namespace pipeline{
    void initialisewindow(int,int); //gets the screenw and screenw
    void initialise(void); //initialises all variables to the known state, i.e start
    void setupuvn(void); //sets up uvn vectors from the camera properties
    void preparemodelviewmatrix(void); //prepares the modelview matrix
    void setprojectionparams(void); //setup's x,top,bottom,left,right
    void prepareprojectionmatrix(void); //prepares projection matrix
    void prepareEyeworldmatrix(const float,const float,const float); //prepares eyeworld matrix, x,y,z are coordinates in 3d
    void prepareclipcoordinatematrix(void); //prepares clipcoordinate matrix
    void preparenormalisedcoordinatematrix(void); //normalises the clip coordinate
    void prepareviewcoordinates(void); //prepares the view coordinates
    void _3dto2d(vertex3d&);
    void ___3dto2d(vertex3d&);
    void intitialvalidvalues(void);
    void adjustchange(void);
    void lighting(vertex3d&);
    void init(void);

    void rotateScene(void);
    void rotateLight(void);
    extern int rotateangle;
    extern int lightangle;
    extern bool enablelight;
    extern bool enableshadow;
    extern bool shadowCodeBegin;
    extern bool wf_rn; //true means wireframe and false means rendered scene
};
#endif
