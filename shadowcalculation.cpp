#include "shadowcalculation.h"
#include "camera.h"
#include "objectreader.h"
#include "pipeline.h"
namespace shadow{
std::string plane;
std::vector<std::string> objects;
std::vector<vertex3d> endpoints;
vertex2d ep1,ep2;
//three vertex of plane taken in antiClockwise dir
vertex3d points_on_plane[3];

//An arbitrary point on plane
vertex3d arb_point;

//Position of Light source
vertex3d light_pos;
//Calculation of Normal to the plane using 3 points on plane
void getplane(){
 object obj = model::loader->getObject(plane);
 //std::cout<<obj.objname;
 if(obj.objname=="NULL"){
     std::cout <<"plane not found"<<std::endl;
     return;
 }
 points_on_plane[0] = obj.faces[0].v[0];
 points_on_plane[1] = obj.faces[0].v[1];
 points_on_plane[2] = obj.faces[0].v[2];

 points_on_plane[0].y += 0.07;
 points_on_plane[1].y += 0.07;
 points_on_plane[2].y += 0.07;

//now calculate endpoints of the plane
//plane object has only two planes

//for(int i=0;i<2;i++){
//    for(int j=0;j<3;j++){
//        vertex3d v = obj.faces[1].v[j];
//        pipeline::___3dto2d(v);
//        endpoints.push_back(v);
//    }
//}
//
//ep1.x = endpoints[0].x;
//ep1.y = endpoints[0].y;
//ep2.x = endpoints[0].x;
//ep2.y = endpoints[0].y;
//
//for(int i=0;i<int(endpoints.size());i++){
//    if(ep1.x>endpoints[i].x){
//        ep1.x = endpoints[i].x;
//    }
//    if(ep2.x < endpoints[i].x){
//        ep2.x = endpoints[i].x;
//    }
//    if(ep1.y>endpoints[i].y){
//        ep1.y = endpoints[i].y;
//    }
//    if(ep2.y < endpoints[i].y){
//        ep2.y = endpoints[i].y;
//    }
//}

//std::cout << endpoints.size(); getchar();
 setarb_point();
}
void setarb_point(){
    arb_point = points_on_plane[0];
    light_pos = cam::light;
    //now calculate the endpoints in 2d
}


Plane_normal determine_plane_coefficients(vertex3d pt[]){

    float A,B,C;
    Plane_normal plane_coeff;

    A=((pt[1].y-pt[0].y)*(pt[2].z-pt[0].z) - (pt[2].y-pt[0].y)*(pt[1].z-pt[0].z));
    B=-((pt[1].x-pt[0].x)*(pt[2].z-pt[0].z) - (pt[2].x-pt[0].x)*(pt[1].z-pt[0].z));
    C=((pt[1].x-pt[0].x)*(pt[2].y-pt[0].y) - (pt[2].x-pt[0].x)*(pt[1].y-pt[0].y));
    //D=-pt[0].x*A-pt[0].y*B-pt[0].z*C;

    plane_coeff.A = A;
    plane_coeff.B = B;
    plane_coeff.C = C;

    return plane_coeff;
}

//Calculation of shadow coordinates
void calculate_shadow_coordinates(vertex3d vertex,vertex3d& shadow_projection)
{
        Plane_normal n = determine_plane_coefficients(points_on_plane);
        //std::cout <<"normal plane: "<< n.A <<' '<<n.B<<' '<<n.C<<std::endl;
        //Vector from light source towards the vertex whose shadow is to be calculated
        vertex3d a(vertex.x - light_pos.x,vertex.y - light_pos.y,vertex.z - light_pos.z);
        //a.show();

        float t =(n.A*(arb_point.x - vertex.x) + n.B*(arb_point.y - vertex.y) + n.C*(arb_point.z - vertex.z))/
              (n.A*(a.x) + n.B*(a.y) + n.C*(a.z));

        float x = vertex.x + (t * a.x);
        float y = vertex.y + (t * a.y);
        float z = vertex.z + (t * a.z);

        shadow_projection.x = x;
        shadow_projection.y = y;
        shadow_projection.z = z;
        //pipeline::___3dto2d(shadow_projection);
        //shadow_projection.show();
//    cout<<endl<<"The shadow points are:"<<endl;
  //Ŝ  cout<<shadow_projection.x<<","<<shadow_projection.y<<","<<shadow_projection.z<<endl;

}

bool isShadowEnabledOn(std::string object){
    for(int i=0;i<int(objects.size());i++){
        if(objects[i]==object){return true;}
    }
    return false;
}

bool isShadowWithinPlane(int x,int y){
    setarb_point();
    std::cout << ep1.x <<' '<<ep1.y<<" , "<<ep2.x<<' '<<ep2.y<<"->"<<x<<' '<<y<<std::endl;
    getchar();
    if((x>=ep1.x&&x<ep2.x)&&(y>=ep1.y&&y<ep2.y)){
        return true;
    }
    return false;
}
};
