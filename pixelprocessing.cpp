#include <iostream>
#include <map>
#include "pipeline.h"
#include "pixelprocessing.h"
#include "buffer.h"
#include "camera.h"
#include "shadowcalculation.h"

namespace processing{
    std::map<int,pixel*> screenbuffer;
}

void processing::processor::addpixel(int x,int y,int z,int r,int g,int b){
    if ((x>=0&&x<cam::screenw)&&(y>=0&&y<cam::screenh)){
        pixel p;
        p.pack(r,g,b,x,y,z);
        fbuffer.push_back(p);
    }
}

void processing::processor::buffertransfer(void){
    std::vector<pixel>::iterator it;
    int x,y,r,g,b;
    float d;
    pixelcolorinfo color;
    for(it=fbuffer.begin();it!=fbuffer.end();++it){
        it->unpack(r,g,b,x,y,d);
        color.set(r,g,b);
        buffer::setbufferpixel(x,y,color);
    }
    fbuffer.clear();
}

///for screen buffer

void processing::addpixel(int& x,int& y,double& z,int& r,int& g,int& b){
    //pixel should be within the screen
    int index = cam::screenw*y+x;
    if ((x>=0&&x<cam::screenw)&&(y>=0&&y<cam::screenh)){

//        if(pipeline::shadowCodeBegin && pipeline::enablelight&&pipeline::enableshadow){
//            if(!shadow::isShadowWithinPlane(x,y)) return;
//
//        }

    //first check if there is pixel in the pos(x,y)
    pixel* pix = processing::screenbuffer[index];
    if(!pix){//there is no pixel added yet, then add the pixel here
        processing::screenbuffer[index] = new pixel(r,g,b,x,y,z);
    }else{ //means we have a pixel already at the place
        //now z buffer will be used to make a decision between two pixels here
        if(z <= pix->z ){
            //means new pixel is near to the screen
            //int R,G,B,X,Y,Z;
            //processing::screenbuffer[index]->unpack(R,G,B,X,Y,Z);
            //std::cout<<R<<' '<<G<<' '<<B<<' '<<X<<' '<<Y<<' '<<Z<<std::endl<<std::endl;

            processing::screenbuffer[index]->pack(r,g,b,x,y,z);

            //processing::screenbuffer[index]->unpack(R,G,B,X,Y,Z);
            //std::cout<<R<<' '<<G<<' '<<B<<' '<<X<<' '<<Y<<' '<<Z<<std::endl<<std::endl;

            //getchar();
        }
        //else old pixel is still closer to screen so do nothing
    }
}

//pixelcolorinfo color;
//color.set(r,g,b);
//buffer::setbufferpixel(x,y,color);
}

void processing::transfertoframebuffer(void){
    int r,g,b,x,y;
    float z;
    pixelcolorinfo color;
    std::map<int,pixel*>::iterator it;
    for(it=processing::screenbuffer.begin();it!=processing::screenbuffer.end();++it){
        (it->second)->unpack(r,g,b,x,y,z); color.set(r,g,b);
        buffer::setbufferpixel(x,y,color);
    }
    processing::screenbuffer.clear();
}
