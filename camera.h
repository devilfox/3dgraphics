#ifndef __CAMERA_H__
#define __CAMERA_H__
#include <Eigen/Dense>
#include "coordinate.h"
#include "lighting.h"
using namespace Eigen;
namespace cam{
    extern vertex3d   eye,lookat,normalisedcoordinates;
    extern uint16_t viewangle,near,far;
    extern double aspectratio,x,top,bottom,left,right;
    extern Vector3d lookup,eyevector,u,v,n;
    extern MatrixXd modelviewmatrix,projectionmatrix,eyeworldmatrix,clipcoordinatematrix;
    extern uint16_t screenw,screenh;
    extern vertex3d out;
    extern vertex3d light;
    extern float ka,kd;
};
#endif
