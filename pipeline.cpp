#include <cmath>
#include "pipeline.h"
#include "camera.h"
#include "coordinate.h"
#include "buffer.h"
#include "pixelprocessing.h"
#include "lighting.h"
#include "shadowcalculation.h"
#include "objectreader.h"
#define PI 3.141592653589793


namespace buffer{pixelcolorinfo* framebuffer;};
namespace processing{processor* intermediatebuffer;}
bool pipeline::enablelight = true;
bool pipeline::enableshadow = true;
bool pipeline::shadowCodeBegin = false;
bool pipeline::wf_rn = false; //set to wireframe at first
int pipeline::rotateangle = 10;
int pipeline::lightangle = 10;
//cam properties:
namespace cam{
    vertex3d   eye,lookat,normalisedcoordinates;
    uint16_t viewangle,near,far;
    double aspectratio,x,top,bottom,left,right;
    Vector3d lookup,eyevector,u,v,n;
    MatrixXd modelviewmatrix(4,4),projectionmatrix(4,4),eyeworldmatrix(4,1),clipcoordinatematrix(4,1);
    uint16_t screenw,screenh;
    vertex3d out;
    vertex3d light;
    float ka,kd;
};
    void pipeline::intitialvalidvalues(void){
        pipeline::initialise();
        cam::viewangle = 45.0;
        cam::near =1.0;
        cam::far = 50.0;
        cam::eye.x = 8;
        cam::eye.y = 10;
        cam::eye.z = 20;
        cam::lookat.x = cam::lookat.y = cam::lookat.z = 0;
        cam::lookup << 0,1,0;
        cam::light.x = -15;
        cam::light.y = 35;
        cam::light.z = 35;
        cam::ka = 0.3;
        cam::kd = 0.8;
        pipeline::setupuvn();
        pipeline::preparemodelviewmatrix();
        pipeline::setprojectionparams();
        pipeline::prepareprojectionmatrix();
        shadow::getplane();
    }

    void pipeline::initialisewindow(int width,int height){
        cam::screenw = width; cam::screenh = height;
        cam::aspectratio = float(cam::screenw)/float(cam::screenh);
        buffer::framebuffer = new pixelcolorinfo[cam::screenh*cam::screenw];
        processing::intermediatebuffer = new processing::processor();
    }

    void pipeline::initialise(void){
        cam::out.x = cam::out.y = cam::lookat.x = cam::lookat.y = cam::lookat.z = cam::eye.x = cam::eye.y = cam::eye.z = 0;
        cam::normalisedcoordinates.x = cam::normalisedcoordinates.y = cam::normalisedcoordinates.z = cam::viewangle = cam::near = cam::far = cam::x = cam::top = cam::bottom = cam::left = cam::right = 0;
        cam::aspectratio = 16/9;
        cam::lookup << 0,0,0; cam::u << 0,0,0; cam::n << 0,0,0;
    }
    void pipeline::setupuvn(void){
        cam::n << (cam::eye.x-cam::lookat.x),(cam::eye.y-cam::lookat.y),(cam::eye.z-cam::lookat.z);
        cam::u = cam::lookup.cross(cam::n); cam::u.normalize();
        cam::n.normalize();
        cam::v = cam::n.cross(cam::u); cam::v.normalize();
    }
    void pipeline::preparemodelviewmatrix(void){
        cam::eyevector << cam::eye.x,cam::eye.y,cam::eye.z;
        cam::modelviewmatrix <<  cam::u[0],    cam::u[1],   cam::u[2],   -cam::eyevector.dot(cam::u),
                                    cam::v[0],    cam::v[1],   cam::v[2],   -cam::eyevector.dot(cam::v),
                                    cam::n[0],    cam::n[1],   cam::n[2],   -cam::eyevector.dot(cam::n),
                                    0.0,             0.0,            0.0,              1;
    }
    void pipeline::setprojectionparams(void){
        cam::x = tan(PI/180.0*cam::viewangle/2.0);
        cam::top = cam::near * cam::x;
        cam::bottom = -cam::top;
        cam::right = cam::top * cam::aspectratio;
        cam::left = -cam::right;
    }
    void pipeline::prepareprojectionmatrix(void){
        cam::projectionmatrix << 2*cam::near/float(cam::right-cam::left),    0.0,    (cam::right+cam::left)/float(cam::right-cam::left),  0.0,
                                    0.0,    2*cam::near/float(cam::top-cam::bottom),  (cam::top+cam::bottom)/float(cam::top-cam::bottom),  0.0,
                                    0.0,    0.0,    -(cam::far+cam::near)/float(cam::far-cam::near),  -(2*cam::far*cam::near)/float(cam::far-cam::near),
                                    0.0,    0.0,    -1, 0;
    }
    void pipeline::prepareEyeworldmatrix(const float x,const float y,const float z){
        MatrixXd point(4,1);
        point << x,
                 y,
                 z,
                 1;
        cam::eyeworldmatrix = cam::modelviewmatrix * point;
    }
    void pipeline::prepareclipcoordinatematrix(void){
        cam::clipcoordinatematrix = cam::projectionmatrix*cam::eyeworldmatrix;
    }
    void pipeline::preparenormalisedcoordinatematrix(void){
        cam::normalisedcoordinates.x = cam::clipcoordinatematrix(0,0)/cam::clipcoordinatematrix(3,0);
        cam::normalisedcoordinates.y = cam::clipcoordinatematrix(1,0)/cam::clipcoordinatematrix(3,0);
        cam::normalisedcoordinates.z = cam::clipcoordinatematrix(2,0)/cam::clipcoordinatematrix(3,0);
    }
    void pipeline::prepareviewcoordinates(void){
        cam::out.x = int(cam::screenw/2+cam::normalisedcoordinates.x*cam::screenw);
        cam::out.y = int(cam::screenh/2+cam::normalisedcoordinates.y*cam::screenh);
        cam::out.z = cam::normalisedcoordinates.z;
    }

    void pipeline::_3dto2d(vertex3d& v){
        //modelview and projection matrix should be genereated by scene
        pipeline::prepareEyeworldmatrix(v.x,v.y,v.z);
        pipeline::prepareclipcoordinatematrix();
        pipeline::preparenormalisedcoordinatematrix();
        pipeline::prepareviewcoordinates();
        if(pipeline::enablelight){
            pipeline::lighting(v);
        }
    }

    void pipeline::___3dto2d(vertex3d& v){
        pipeline::_3dto2d(v);
        v.x = cam::out.x;
        v.y = cam::out.y;
        v.z = cam::out.z;
    }

    void pipeline::adjustchange(){
        pipeline::setupuvn();
         pipeline::preparemodelviewmatrix();
         pipeline::setprojectionparams();
         pipeline::prepareprojectionmatrix();
         shadow::setarb_point();
    }

    void pipeline::lighting(vertex3d& v){


        MatrixXd mod(4,4),tmod(4,4);
        mod = cam::modelviewmatrix.inverse();
        tmod = mod.transpose();
        MatrixXd normal(4,1);

        normal << v.nx,
                  v.ny,
                  v.nz,
                  1;

        MatrixXd fm(4,1);
        fm = tmod*normal;

        //std::cout << cam::eyeworldmatrix<<std::endl<<std::endl;
        lighting::lightobj.setposition(cam::light.x,cam::light.y,cam::light.z);
        lighting::lightobj.calculate_light_vector(cam::eyeworldmatrix(0,0),cam::eyeworldmatrix(1,0),cam::eyeworldmatrix(2,0));
        //std::cout << lighting::lightobj.modelled_position.x<<' '<< lighting::lightobj.modelled_position.y<<' '<< lighting::lightobj.modelled_position.z<<std::endl<<std::endl;
        //std::cout << lighting::lightobj.light_vector.x<<' '<< lighting::lightobj.light_vector.y<<' '<< lighting::lightobj.light_vector.z<<std::endl<<std::endl;

    //getchar();
        Vector3d fmv,ln;
        fmv << fm(0,0),fm(1,0),fm(2,0);
        ln << lighting::lightobj.light_vector.x,lighting::lightobj.light_vector.y,lighting::lightobj.light_vector.z;
//
        float cosine;
        int ar,ag,ab;
        cosine = fmv.dot(ln);

        //std::cout << fmv<<std::endl<<std::endl<<ln<<std::endl<<std::endl<<cosine<<std::endl<<std::endl; getchar();
//
        if(cosine<0){cosine=0;}
        //ambient light
        ar = v.r*cam::ka; ag = v.g*cam::ka; ab = v.b*cam::ka;
        //diffused
        v.r = v.r*cam::kd*cosine+ar; v.g = v.g*cam::kd*cosine+ag; v.b = v.b*cam::kd*cosine+ab;
        if(v.r>255){v.r=255;} if(v.g>255){v.g=255;} if(v.b>255){v.b=255;}
    }

void pipeline::rotateScene(){
    float x = cam::eye.x, y = cam::eye.y, z = cam::eye.z,_x,_y,_z;
    float rad = PI/180.0*float(pipeline::rotateangle);
    _z = z*cos(rad)-x*sin(rad);
    _x = z*sin(rad)+x*cos(rad);
    _y = y;
    cam::eye.x = _x; cam::eye.y = _y; cam::eye.z = _z;
}

void pipeline::rotateLight(){
    float x = cam::light.x, y = cam::light.y, z = cam::light.z,_x,_y,_z;
    float rad = PI/180.0*float(pipeline::lightangle);
    _z = z*cos(rad)-x*sin(rad);
    _x = z*sin(rad)+x*cos(rad);
    _y = y;
    cam::light.x = _x; cam::light.y = _y; cam::light.z = _z;
}

void pipeline::init(){
    if(!model::loader->load("./bcourt.obj")){return;}

    shadow::plane = "o Plane";
    shadow::objects.push_back("o Cylinder.002_Cylinder.000");
    shadow::objects.push_back("o Cylinder.003");
    shadow::objects.push_back("o Cylinder_Cylinder.001");
    shadow::objects.push_back("o Torus.000");
    shadow::objects.push_back("o Cube.001");
    shadow::objects.push_back("o Cylinder.001_Cylinder.002");
    shadow::objects.push_back("o Cube.000_Cube.002");
    shadow::objects.push_back("o Cylinder_Cylinder.001");
    shadow::objects.push_back("o Torus.002");
    shadow::objects.push_back("o Cylinder.002_Cylinder.000");

}
