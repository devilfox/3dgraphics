#include "buffer.h"
#include "camera.h"
#include<GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>

void pixelcolorinfo::set(int& r,int& g,int& b){
        red=r; green=g; blue=b;
}
void pixelcolorinfo::set(pixelcolorinfo &p){
        red=p.red; green=p.green; blue=p.blue;
}
bool pixelcolorinfo::operator !=(pixelcolorinfo& p){
        if(red!=p.red||green!=p.green||blue!=p.blue){return true;}
        return false;
}
void buffer::loadframebuffer(void){
    glReadPixels(0,0,cam::screenw,cam::screenh,GL_RGB,GL_UNSIGNED_BYTE,buffer::framebuffer);
}
void buffer::writeframebuffer(void){
    glDrawPixels(cam::screenw,cam::screenh,GL_RGB,GL_UNSIGNED_BYTE,buffer::framebuffer);
}
void buffer::setbufferpixel(int& x,int& y,pixelcolorinfo& color){
    if ((x>=0&&x<cam::screenw)&&(y>=0&&y<cam::screenh)){
        buffer::framebuffer[cam::screenw*y+x]=color;
    }
}
