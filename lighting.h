#ifndef __LIGHT_H__
#define __LIGHT_H__
#include <Eigen/Dense>
#include "camera.h"
#include "coordinate.h"
using namespace Eigen;

class LightSource
{
    public:
    vertex3d position; //Position in world coordinate
    vertex3d modelled_position; //Position in eye coordinate
    vertex3d light_vector; //Normal vector towards vertex
    float r,g,b;

    LightSource();

    LightSource(int x,int y,int z,float _r,float _g,float _b);

    void setposition(int x,int y,int z);

    void setposition(vertex3d pos);


    void setintensity(float _r,float _g,float _b);


    void calculate_eye_coordinates();


    vertex3d getEyeCoordinates();


    void calculate_light_vector(float x,float y,float z);


    void calculate_light_vector(vertex3d v);

};

namespace lighting{
    extern LightSource lightobj;
}
#endif
