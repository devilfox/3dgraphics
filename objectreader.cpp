#include "objectreader.h"
#include "coordinate.h"
#include <fstream>
#include <vector>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
namespace model{
bool objloader::load(std::string model){
    std::fstream Model(model.c_str());
    if(!Model){return false;}
    object obj;
    std::string buffer,diff;
    std::vector<vertex3d> vertex,normal;
    bool sflag = false;
    while(!Model.eof()){
        diff.clear();
        buffer.clear();
        getline(Model,buffer);
        diff += buffer[0];
        diff += buffer[1];

        if(diff=="o "){
            if(!sflag){sflag = true;obj.objname = buffer;}
            else{
                objects.push_back(obj);
                obj.faces.clear();
                obj.objname = buffer;
            }
        }
        else if(diff=="v "){
            // v f f f
            vertex3d v;
            std::sscanf(buffer.c_str(),"v %f %f %f",&v.x,&v.y,&v.z);
            vertex.push_back(v);

        }else if(diff=="vn"){
            //vn f f f
            vertex3d vn;
            std::sscanf(buffer.c_str(),"vn %f %f %f",&vn.x,&vn.y,&vn.z);
            normal.push_back(vn);

        }else if(diff=="f "){
            if(has(buffer,"//")){ //f int//int (f v//vn)
                int v1,v2,v3,d;
                std::sscanf(buffer.c_str(),"f %d//%d %d//%d %d//%d",&v1,&d,&v2,&d,&v3,&d);
                face f;
                f.vid.push_back(v1);
                f.vid.push_back(v2);
                f.vid.push_back(v3);
                f.v.push_back(vertex[v1-1]);
                f.v.push_back(vertex[v2-1]);
                f.v.push_back(vertex[v3-1]);
                f.n = normal[d-1];
                obj.faces.push_back(f);
            }else if(has(buffer,"/")){ //f int/int/int

            }else if(has(buffer," ")){ //f int int both are vertices
                int v1,v2;
                std::sscanf(buffer.c_str(),"f %d %d",&v1,&v2);
                face f;
                f.v.push_back(vertex[v1-1]);
                f.v.push_back(vertex[v2-1]);
                f.n.x = f.n.y = f.n.z = 0;
                obj.faces.push_back(f);

            }
        }
    }
    objects.push_back(obj);
    resolvenormals();
    Model.close();
    return true;
}

bool objloader::has(std::string& str,std::string substr){
    std::size_t found = str.find(substr);
    if(found!=std::string::npos){return true;}
    return false;
}

void objloader::resolvenormals(){
    std::vector<object>::iterator it;
    std::vector<face>::iterator _it;
    vertex3d normal;
    int r,g,b;
    srand(time(NULL));
    for(it=model::loader->objects.begin();it!=model::loader->objects.end();++it){
            getcolor(it->objname,r,g,b);
            it->color(r,g,b);
        for(_it=it->faces.begin();_it!=it->faces.end();++_it){
            if(_it->v.size()==3){
                normal = _it->n;
                for(int i=0;i<3;i++){
                    if(!vertextracker[_it->vid[i]]){
                        tracker* track = new tracker(_it->v[i]);
                        track->update(normal);
                        vertextracker[_it->vid[i]] = track;
                    }else{
                        vertextracker[_it->vid[i]]->update(normal);
                    }
                    _it->v[i].color(r,g,b);
                }
            }else{
                    for(int i=0;i<2;i++){
                        _it->v[i].color(r,g,b);
                    }
                }
        }
    }
    //now averate them
    std::map<int,tracker*>::iterator mit;
    for(mit=vertextracker.begin();mit!=vertextracker.end();++mit){
        mit->second->average();
    }
    //now update these values to the vertices used in each face
    for(it=model::loader->objects.begin();it!=model::loader->objects.end();++it){
        for(_it=it->faces.begin();_it!=it->faces.end();++_it){
            if(_it->v.size()==3){
                for(int i=0;i<3;i++){
                    _it->v[i].nx = vertextracker[_it->vid[i]]->v.nx;
                    _it->v[i].ny = vertextracker[_it->vid[i]]->v.ny;
                    _it->v[i].nz = vertextracker[_it->vid[i]]->v.nz;
                }
            }
        }
    }
}

object objloader::getObject(std::string name){
    std::vector<object>::iterator it;
    for(it=objects.begin();it!=objects.end();++it){
        //std::cout<<it->objname;
        if(it->objname == name){return *it;}
    }
    object obj;
    obj.objname = "NULL";
    return obj;
}


void objloader::getcolor(std::string name,int& r,int& g,int& b){
    if(name=="o Cylinder.002_Cylinder.000" || name=="o Cylinder.003" || name=="o Cylinder_Cylinder.001" || name=="o Cylinder.001_Cylinder.002" || name=="o Cylinder_Cylinder.001" || name=="o Cylinder.002_Cylinder.000"){
        //pole
        r = 178; g = 103; b = 34;
    }
    else if(name=="o Cube.001" || name=="o Cube.000_Cube.002"){
     //basketPlane
        r = 68; g = 37; b = 37;
    }else if(name=="o Plane"){
    //plane
        r = 187;
        b = 196;
        g = 215;
    }else if(name=="o Torus.000"||name=="o Torus.002"){
        r=255;
        g=255;
        b=255;
    }
}
  objloader* loader = new objloader();
};
